export default function rupiahHelper(number) {
    const convertToRupiah = () => {
        return "Rp " + number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    };

    return {
        convertToRupiah
    }
}